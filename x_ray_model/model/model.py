import numpy as np
from ..common import constants
from ..common.utils import logger as log
from keras import layers, models
from sklearn.metrics import confusion_matrix, roc_curve, classification_report

class Model:
    
    def __init__(self, dataset):
        self.__model = Model.__define_model()
        self.__trained_model = self.__get_trained_model(dataset)
        self.__results = self.__get_model_results(dataset)

    @property
    def model(self):
        return self.__model

    @property
    def trained_model(self):
        return self.__trained_model

    @property
    def results(self):
        return self.__results

    def __get_trained_model(self, dataset):

        log.info("Training model", splitter=True)

        trained_model = self.model.fit(
            dataset.train_images,
            dataset.train_labels,
            batch_size=constants.BATCH_SIZE,
            epochs=constants.EPOCHS,
            verbose=1,
            validation_split=constants.VALIDATION_SPLIT,
            callbacks=constants.CALLBACKS
        )

        log.info('The model has ended the training')

        return trained_model
    
    def __get_model_results(self, dataset):
    
        log.info('Getting model results...', splitter=True)

        model_results = {
            'Model history': self.trained_model.history,
            'Test results': self.model.evaluate(dataset.test_images, dataset.test_labels, return_dict=True),
            'Model test predictions': self.model.predict(dataset.test_images)
        }

        model_results['Confusion matrix'] = self.__get_confusion_matrix(
            model_results['Model test predictions'],
            dataset.test_labels,
            ['Not COVID', 'COVID']
        )

        model_results['ROC curve'] = self.__get_roc_curve(
            dataset.test_labels,
            model_results['Model test predictions']
        )

        return model_results
    
    @staticmethod
    def __define_model():
        model = models.Sequential()

        model.add(layers.Conv2D(
            32,
            constants.CONV_WINDOW_SIZE,
            activation=constants.CONV_ACTIVATION,
            input_shape=constants.IMAGES_SHAPE,
            padding=constants.PADDING,
            kernel_initializer=constants.WEIGHT_INITIALIZER))
        model.add(layers.MaxPooling2D(
            constants.POOLING_SIZE, padding=constants.POOLING_PADDING))
        model.add(layers.Conv2D(
            64,
            constants.CONV_WINDOW_SIZE,
            activation=constants.CONV_ACTIVATION,
            padding=constants.PADDING,
            kernel_initializer=constants.WEIGHT_INITIALIZER))
        model.add(layers.MaxPooling2D(
            constants.POOLING_SIZE, padding=constants.POOLING_PADDING))
        model.add(layers.Conv2D(
            128,
            constants.CONV_WINDOW_SIZE,
            activation=constants.CONV_ACTIVATION,
            padding=constants.PADDING,
            kernel_initializer=constants.WEIGHT_INITIALIZER))
        model.add(layers.MaxPooling2D(
            constants.POOLING_SIZE, padding=constants.POOLING_PADDING))
        model.add(layers.Flatten())
        model.add(layers.Dense(1, activation='sigmoid'))

        model.compile(loss=constants.LOSS,
                      optimizer=constants.OPTIMIZER,
                      metrics=['accuracy'])

        return model
    
    @staticmethod
    def __get_confusion_matrix(model_test_predictions, test_labels, labels_info):
        normalized_model_test_predictions = np.round(model_test_predictions)

        conf_matrix = confusion_matrix(
            test_labels, normalized_model_test_predictions)

        results = {
            'report': classification_report(test_labels, normalized_model_test_predictions, target_names=labels_info),
            'true_positives': conf_matrix[0][0],
            'false_positives': conf_matrix[0][1],
            'false_negatives': conf_matrix[1][0],
            'true_negatives': conf_matrix[1][1],
            'precision': (conf_matrix[0][0])/(conf_matrix[0][0] + conf_matrix[0][1]),
            'recall': (conf_matrix[0][0])/(conf_matrix[0][0] + conf_matrix[1][0]),
            'accuracy': (conf_matrix[0][0] + conf_matrix[1][1])/(conf_matrix[0][0] + conf_matrix[1][1] + conf_matrix[0][1] + conf_matrix[1][0]),
            'specificity': (conf_matrix[1][1])/(conf_matrix[1][1] + conf_matrix[0][1])
        }

        return results

    @staticmethod
    def __get_roc_curve(test_labels, model_test_predictions):
        results = {}
        results['specificity'], results['sensitivity'], _ = roc_curve(
            test_labels, model_test_predictions)

        return results