import matplotlib.pyplot as plt
import seaborn as sns
from ...common.utils import logger as log


def plot_confusion_matrix(data):

    CONFUSION_MATRIX = [
        [data['true_positives'], data['false_positives']],
        [data['false_negatives'], data['true_negatives']]
    ]

    LABELS = [
        '\nTrue positives',
        '\nFalse positives',
        '\nFalse negatives',
        '\nTrue negatives'
    ]

    conf_display = sns.heatmap(
        CONFUSION_MATRIX, annot=True, fmt='d', cmap='Blues')
    conf_display.set_title("Confusion matrix")

    # Sets a label in each cell of the heatmap
    for label_index, element in enumerate(conf_display.texts):
        element.set_text(element.get_text() + LABELS[label_index])

    log.info('Classification report:', data['report'])

def plot_roc_curve(data):

    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(data['specificity'], data['sensitivity'])
    plt.xlabel('1 - Specificity')
    plt.ylabel('Sensitivity')
    plt.title('X-ray model ROC curve')
    plt.legend()
    plt.show()


def plot_accuracy(history):

    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.plot(history['accuracy'])
    plt.plot(history['val_accuracy'])
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()


def plot_loss(history):

    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()

def plot_results(test_samples, test_labels, results):
    
    plot_accuracy(results['Model history'])
    plot_loss(results['Model history'])
    plot_roc_curve(results['ROC curve'])
    plot_confusion_matrix(results['Confusion matrix'])
    # show_failed_test_samples(test_samples, test_labels, np.round(results['Model test predictions']))

def show_failed_test_samples(test_samples, test_labels, predicted_labels, channel='RGB'):
    
    false_negatives, false_positives = get_falses(test_samples, test_labels, predicted_labels)
    
    plot_images(false_negatives, 'false negatives')
    plot_images(false_positives, 'false positives')

def get_falses(test_samples, test_labels, predicted_labels):
    
    false_negatives = []
    false_positives = []

    log.info('Getting false negatives and positives...')

    for index, right_label in enumerate(test_labels):
    
        labels_are_different = (right_label != predicted_labels[index])
        if(labels_are_different):
            sample_is_covid = (predicted_labels[index] == 1)
            if(sample_is_covid):
                false_positives.append(test_samples[index])
            else:
                false_negatives.append(test_samples[index])
    
    log.info('Falses report:', '- False negatives: {}\n- False positives: {}'.format(len(false_negatives), len(false_positives)))

    return false_negatives, false_positives

def plot_images(images, images_title = ''):

    plt.rcParams.update({'figure.max_open_warning': 0})

    if(len(images) > 0):
        if(images_title != ''): log.info('Plotting {}'.format(images_title))
        for image in images:
            plt.figure()
            plt.imshow(image)