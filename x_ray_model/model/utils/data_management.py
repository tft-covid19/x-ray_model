import pandas as pd
from ...common.utils import logger as log
from ...common.constants import (
    TRAIN_IMAGES_FILE_PATH,
    TRAIN_LABELS_FILE_PATH,
    TEST_IMAGES_FILE_PATH,
    TEST_LABELS_FILE_PATH,
    CONV_WINDOW_SIZE,
    POOLING_SIZE,
    POOLING_STRIDE,
    PADDING,
    POOLING_STRIDE,
    EPOCHS,
    BATCH_SIZE,
    IMAGES_SHAPE,
    OPTIMIZER,
    LOSS,
    VALIDATION_SPLIT,
    CONV_ACTIVATION,
    LOG_PATH
)
from datetime import datetime
from IPython.display import display
from pickle import dump, load


def variable_to_file(variable, file_path):
    
    log.info('Inserting data into the file: {}'.format(file_path), splitter=True)

    storage_file = open(file_path, 'wb')
    dump(variable, storage_file)
    storage_file.close()

    log.info('The data has been inserted successfully')


def load_variable_from_file(file_path):
    
    load_file = open(file_path, 'rb')
    loaded_var = load(load_file)
    load_file.close()

    return loaded_var


def dataset_images_to_files(dataset):
    
    variable_to_file(dataset.train_images, TRAIN_IMAGES_FILE_PATH)
    variable_to_file(dataset.train_labels, TRAIN_LABELS_FILE_PATH)
    variable_to_file(dataset.test_images, TEST_IMAGES_FILE_PATH)
    variable_to_file(dataset.test_labels, TEST_LABELS_FILE_PATH)


def load_data(dataset):

    log.info('Retrieving all the dataset from files')

    dataset.train_images = load_variable_from_file(TRAIN_IMAGES_FILE_PATH)
    dataset.train_labels = load_variable_from_file(TRAIN_LABELS_FILE_PATH)
    dataset.test_images = load_variable_from_file(TEST_IMAGES_FILE_PATH)
    dataset.test_labels = load_variable_from_file(TEST_LABELS_FILE_PATH)

    log.info('All the dataset has been obtained successfully')


def add_record_to_log(results, description):

    if(description == ''):
        description = input('Set a description for the test: ')

    model_info = """    Kernel size: {}
    Pooling size: {}
    Pooling stride: {}
    Padding: {}
    Pooling padding: {}
    Epochs: {}
    Batch size: {}
    Image shape: {}
    Optimizer: {}
    Loss type: {}
    Validation split: {}
    Activation function: {}""".format(
        CONV_WINDOW_SIZE,
        POOLING_SIZE,
        POOLING_STRIDE,
        PADDING,
        POOLING_STRIDE,
        EPOCHS,
        BATCH_SIZE,
        IMAGES_SHAPE,
        OPTIMIZER,
        str(LOSS),
        VALIDATION_SPLIT * 100,
        CONV_ACTIVATION)

    new_row = [
        datetime.now(),
        model_info,
        results['Test results']['accuracy'] * 100,
        results['Test results']['loss'] * 100,
        results['Confusion matrix']['true_positives'],
        results['Confusion matrix']['false_positives'],
        results['Confusion matrix']['false_negatives'],
        results['Confusion matrix']['true_negatives'],
        results['Confusion matrix']['precision'] * 100,
        results['Confusion matrix']['recall'] * 100,
        results['Confusion matrix']['accuracy'] * 100,
        results['Confusion matrix']['specificity'] * 100,
        description
    ]

    dataframe = pd.read_csv(LOG_PATH)
    number_of_rows = dataframe.shape[0]
    better_false_negative_result_found = (dataframe['FALSE N.'].min() > results['Confusion matrix']['false_negatives'])

    if (better_false_negative_result_found or number_of_rows == 0):
        insert_new_row_in_log(new_row, dataframe)
    else:
        add_row_prompt = input(
            'The record is not better than any of the log, do you still want to save it? [(yes)/no]: ')

        # Keeps prompting until the answer is one of the options
        while (add_row_prompt != 'yes' and add_row_prompt != 'no' and add_row_prompt != ''):
            add_row_prompt = input(
                'Please write \'yes\' or \'no\': ')

        insertion_is_confirmed = (add_row_prompt == 'yes' or add_row_prompt == '')
        if(insertion_is_confirmed):
            insert_new_row_in_log(new_row, dataframe)


def insert_new_row_in_log(row, dataframe):

    new_dataframe_row = pd.Series(row, index=dataframe.columns)
    dataframe = dataframe.append(new_dataframe_row, ignore_index=True)

    dataframe.to_csv(LOG_PATH, index=False)


def show_sorted_columns(columns):
    
    pd.options.display.max_rows = 10
    dataframe = pd.read_csv(LOG_PATH).sort_values(columns)
    display(dataframe[columns])
