import tensorflow as tf
from ..common.utils.keras_callbacks import (
    STOP_IF_NOT_IMPROVING, 
    AUTO_REDUCE_LR_IF_NOT_IMPROVING, 
    STOP_ON_NAN_IN_LOSS
)

# DATASETS
DATASET_1_1 = {"path": "/content/gdrive/MyDrive/ULPGC/TFT/data/dataset-1/Dataset All Augmented/Dataset All Augmented/",
               "name": "Dataset 1.1",
               "classes": ["COVID-19", "Non-COVID-19"]}
DATASET_1_2 = {"path": "/content/gdrive/MyDrive/ULPGC/TFT/data/dataset-1/Dataset COVID-19 Augmented/Dataset COVID-19 Augmented/",
               "name": "Dataset 1.2",
               "classes": ["COVID-19", "Non-COVID-19"]}

# IMAGES
IMAGE_SIZE = 200
TRAIN_IMAGES_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_1/train_images'
TRAIN_LABELS_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_1/train_labels'
TEST_IMAGES_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_1/test_images'
TEST_LABELS_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_1/test_labels'

# MODEL PARAMETERS
CONV_WINDOW_SIZE = (5, 5)
POOLING_SIZE = (2, 2)
IMAGES_SHAPE = (200, 200, 3)
EPOCHS = 60
PADDING = 'same'
POOLING_PADDING = 'same'
POOLING_STRIDE = (2, 2)
BATCH_SIZE = 50
OPTIMIZER = 'Adam'
LOSS = tf.keras.losses.BinaryCrossentropy()
VALIDATION_SPLIT = 0.2
CONV_ACTIVATION = 'PReLU'
WEIGHT_INITIALIZER = tf.keras.initializers.TruncatedNormal()
CALLBACKS = [STOP_IF_NOT_IMPROVING,
             AUTO_REDUCE_LR_IF_NOT_IMPROVING,
             STOP_ON_NAN_IN_LOSS]

# CONV_ACTIVATION = tf.keras.layers.PReLU() # Will not work if it's not calling in each layer

# OTHER
LOG_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/projects/X-Ray_Model/log/Dataset 1.1_weights_initializers_tests.csv'
MODEL_CHECKPOINT_FILE = '/content/gdrive/MyDrive/ULPGC/TFT/projects/X-Ray_Model/log/checkpoints/model_checkpoint'
