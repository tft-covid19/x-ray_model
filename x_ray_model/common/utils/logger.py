import datetime

def info(message, extra_data=None, splitter=False):

    if(splitter):
        print('*************************************************')

    print('INFO {} => {}'.format(datetime.datetime.now().strftime('[%x %X]'), message))
    
    if extra_data:
        print(extra_data)


def error(error):
    
    print('#################################################')
    print('ERROR {} => {}'.format(datetime.datetime.now().strftime('[%x %X]'), error))
    print('#################################################')
